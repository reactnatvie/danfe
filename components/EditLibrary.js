import React, { Component } from "react";
import { View, Text, Image, Button, Alert,StyleSheet,TextInput } from "react-native";

export default class EditLibrary extends Component {
    render() {
      return (
          <View style={styles.container}>
        <View
        style={{
          height: 40,
          width: 250,
          borderRadius: 10,
          flexDirection: "row",
          marginTop: 30,
          alignItems: "center",
          justifyContent: "center",
          marginLeft: 25
        }}
      >
      <View style={styles.buttonStyle}>
        <Button
          onPress={() => {
            Alert.alert("You tapped the Add button!");
          }}
          title="Add"
        />
        </View>
        <View style={styles.buttonStyle}>
        <Button
          onPress={() => {
            Alert.alert("You tapped the Edit  button!");
          }}
          title="Edit"
        />
        </View>
        <View style={styles.buttonStyle}>
          <Button
            onPress={() => {
              Alert.alert("You tapped the Remove button!");
            }}
            title="Remove"
          />
          </View>
      </View>
      <View style={styles.viewInnerStyle}>
            <Text style={{ color: "black", fontSize: 18 }}>Rose</Text>
          <Image
            style={{ width: 100, height: 100, marginLeft: 25, marginTop: 20 }}
            source={require("../images/rose.png")}
          />
          
        </View>
    </View>
      );
    }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#ffffff",
      justifyContent: "center",
      alignItems: "center",
      
    },
    viewStyle :{
        height: 50,
        width: 250,
        borderRadius: 10,
        backgroundColor: "#00aa00",
        marginBottom: 25
    },
    buttonStyle :{
        height: 50,
        width: 100,
        borderRadius: 10,
        backgroundColor: "#00aa00",
        marginBottom: 25
    },
    viewInnerStyle : {
        height: 150,
        width: 150,
        justifyContent: "flex-start",
        borderColor: "#0feb0f",
        borderStyle: "dotted",
        borderWidth: 2,
        borderRadius: 1,
        position: "relative",
        marginTop: 30,
        marginLeft: 10
     
    },
    textStyle :
    {
      fontSize : 20,
      color: "white",
      marginTop: 10,
      marginLeft: 30
    }, 
  
  });
