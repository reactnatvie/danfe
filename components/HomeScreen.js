import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  Alert,
  ScrollView,
  TouchableOpacity,
} from "react-native";
// import { createStackNavigator, createAppContainer } from "react-navigation";
import { db } from './FirebaseConfig';
import ItemComponent from '../src/ItemComponent';
import RNFetchBlob from 'react-native-fetch-blob';

let libraryRef = db.ref('/Library');

export class HomeScreen extends React.Component {
  _onAddLibrary=()=> {
    this.props.navigation.navigate('Library');
  };
  state = {
    items: []
}
componentDidMount() {
  libraryRef.on('value', (snapshot) => {
      let data = snapshot.val();
      let items = Object.values(data);
      this.setState({items});
      console.log(items)
   });
}
  // _onEditLibrary=()=> {
  //   this.props.navigation.navigate('EditLibrary');
  // };
  render() {
    const {navigate} = this.props.navigation;
    console.log('ITems Length.....',this.state.items.length);
    console.log('ITems Length.....',this.state.items);
    return (
      <View>
        <Text style={ {marginTop:10, marginBottom:50, fontSize:20 }}>  Hello user welcome to to your libraries! </Text>
        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
          <View style={styles.viewStyle}>
          <TouchableOpacity  onPress={this._onAddLibrary}>
            <Image
              style={{ width: 100, height: 100, justifyContent: "center" }}
              source={require("../images/plus.png")}
            />
            <Text style={styles.textStyle}> Add Library </Text>
            </TouchableOpacity >
          </View>
          {/* <View style={styles.viewInnerStyle}>
          <TouchableOpacity  onPress={this._onEditLibrary}>
            <View >
              <Text style={styles.textStyle}>  Library One </Text>
            </View>
            </TouchableOpacity>
          </View> */}
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            marginTop: 50
          }}
        >
          <View style={styles.viewInnerStyle}>
            {
                    this.state.items.length > 0
                    ?
                <ItemComponent items={this.state.items} />
            : <Text>No items</Text>
            }
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
    justifyContent: "center",
    alignItems: "center"
  },
  viewStyle :{
    marginLeft:10,
    marginRight:10,
    width: 150,
    height: 150,
    alignItems:"center",
    justifyContent: "center",
    justifyContent: "flex-start",
    borderColor: "#0feb0f",
    borderStyle: "dotted",
    borderWidth: 2,
    borderRadius: 1,
  },
  viewInnerStyle : {
    flexDirection: "column",
    marginLeft:10,
    marginRight:10,
    width: 150,
    height: 150,
    // backgroundColor: "#0feb0f",
    alignItems:"center",
    justifyContent: "center",
   
  },
  textStyle :
  {
    fontSize : 20,
    color: "black",
  }, 

});


export default HomeScreen;