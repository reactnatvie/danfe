import React, {Component} from 'react';
import { createStackNavigator, createAppContainer } from "react-navigation";
import HomeScreen from './HomeScreen';
import AddLibrary from "./AddLibrary";
// import { EditLibrary } from './EditLibrary';


const AppNavigator=createStackNavigator({
    Home : { screen : HomeScreen },
    Library : {screen:AddLibrary},
    // EditLibrary:{screen:EditLibrary},
    });
    const App = createAppContainer(AppNavigator);
    export default App;