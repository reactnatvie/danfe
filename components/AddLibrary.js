import React, { Component } from "react";
import { View, Text, Image, Button, Alert,StyleSheet,TextInput,TouchableOpacity } from "react-native";
import ImagePicker from 'react-native-image-picker';
import {LibraryService} from '../Services/LibraryService';
import renderIf from './renderif';

export default class AddLibrary extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      ImageSoruce:null,
      status:true,
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(e) {
    this.setState({ 
      name: e.nativeEvent.text
    });
  }
  handleSubmit() {
    LibraryService(this.state.name,this.state.ImageSoruce);
    Alert.alert(
      'Item saved successfully'
     );
  }
  takePicture(){
    const options={
      quality:1.0,
      maxwidth:500,
      maxHeight:500,
      storageOptions:{
        skipBack:true
      }
    };

  ImagePicker.showImagePicker(options, (response) => {
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled image picker');
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    } else {
      let source = { uri: response.uri };
      // You can also display the image using data:
      // const source = { uri: 'data:image/jpeg;base64,' + response.data };

      this.setState({
        ImageSoruce: source,
      });
    }
  });
  this.setState({
    status:!this.state.status
  });
}

  render() {
    return (
      <View style={styles.container}>
        <Text style={{ fontSize: 30, marginBottom: 50}}>
          Creating a new Library
        </Text>
        <View style={styles.viewStyle}>
          <TextInput editable = {true} placeholder="Enter Name for Library" style={styles.textStyle} onChange={this.handleChange}>
          
          </TextInput>
        </View>
        <View style={styles.viewInnerStyle}>
        { this.state.status ?
        <TouchableOpacity onPress={this.takePicture.bind(this)}  style={styles.capture}>
          <Image
            style={{ width: 150, height: 150}}
            source={require("../images/plus.png")}
            />
            <View><Text style={{ color: "black", fontSize: 18}}>Add Library</Text></View>
          </TouchableOpacity> :
          <Image
          style={{ width: 200, height: 200}}
          source={this.state.ImageSoruce}/>
        }
        
        </View>
        <View
          style={{
            height: 40,
            width: 250,
            borderRadius: 10,
            flexDirection: "row",
            marginTop: 30,
            alignItems: "center",
            justifyContent: "center",
            marginLeft: 25
          }}
        >
        <View style={styles.buttonStyle}>
          <Button
            onPress={() => {
              Alert.alert("You tapped the cancel button!");
            }}
            title="Cancel"
          />
          </View>
          <View style={styles.buttonStyle}>
          <Button
            onPress={this.handleSubmit}
            title="Save"
          />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#ffffff",
      justifyContent: "center",
      alignItems: "center",
      
    },
    capture :{
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
    },
    viewStyle :{
        height: 50,
        width: 250,
        borderRadius: 10,
        backgroundColor: "#00aa00",
        marginBottom: 15
    },
    buttonStyle :{
        height: 50,
        width: 100,
        borderRadius: 10,
        backgroundColor: "#00aa00",
        marginBottom: 25
    },
    viewInnerStyle : {
        height: 200,
        width: 200,
        justifyContent: "flex-start",
        borderColor: "#0feb0f",
        borderStyle: "dotted",
        borderWidth: 2,
        borderRadius: 1,
        position: "relative",
        marginTop: 30,
        marginLeft: 10
     
    },
    textStyle :
    {
      fontSize : 20,
      color: "white",
      marginTop: 10,
      marginLeft: 20
    }, 
  
  });
