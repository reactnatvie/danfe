// ItemComponent.js

import React, { Component } from 'react';
import {  View, Text, StyleSheet,Image} from 'react-native';
import PropTypes from 'prop-types';
import { blockStatement } from '@babel/types';

const styles = StyleSheet.create({
    itemsList: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-around',
    },
    itemtext: {
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
        color: "black",
    }
});

export default class ItemComponent extends Component {

  static propTypes = {
      items: PropTypes.array.isRequired
  };

  render() {
    return (
      <View style={styles.itemsList}>
        {this.props.items.map((item, index) => {
            return (
                <View key={index}>
                    <Text style={styles.itemtext}>{item.Name}</Text>
                    <Image source={item.Image}></Image>
                </View>
            )
        })}
      </View>
    );
  }
}