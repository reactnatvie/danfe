/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import RNAccountKit from 'react-native-facebook-account-kit'
import Axios from 'axios';
import AppNavigator from './components/AppNavigator';

export default class App extends Component{

  constructor(){
    super();
    this.state ={
      phoneNumber:''
    }

  }
// componentDidMount(){
 
// }

  sendRequestForPhoneNumber = async(token) =>{
    console.log('Get Started to get phone number',token);
    try {
    var access_token_data = ['AA','411550169391044','a702bb4d451b8fb8713bf6475bda7dd1']
    console.log(access_token_data);
    var access_token = access_token_data.join('|');
    console.log(access_token);
    var token_exchange_url = 'https://graph.accountkit.com/v1.1/access_token?grant_type=authorization_code&code=${token}&access_token=${access_token}';
    console.log(token_exchange_url);
    let getTokens = await Axios(token_exchange_url);
    console.log(getTokens);
    let  getDetailsUrl ='https://graph.accountkit.com/v1.1/me?access_token=${getTokens.data.access_token}';
    let getDetauilsUser= await Axios(getDetailsUrl);
    return getDetauilsUser.data;
} catch (error) {
  console.log('Error from catch block',error);
}
  
  }
  render() {
    RNAccountKit.configure({
      responseType: 'code', // 'token' by default,
      initialPhoneCountryPrefix: '+977', // autodetected if none is provided
      // receiveSMS: true|false, // true by default,
      // countryWhitelist: ['AR'], // [] by default
      // countryBlacklist: ['US'], // [] by default
      defaultCountry: 'NP',
      // theme: {...}, // for iOS only, see the Theme section
      // viewControllerMode: 'show'|'present' // for iOS only, 'present' by default
      // getACallEnabled: true|false
    })
    // const AppContainer = createAppContainer(AppNavigator);
      // Shows the Facebook Account Kit view for login via SMS
  RNAccountKit.loginWithPhone()
  .then(async(token) => {
    if (!token) {
      console.log('Login cancelled');
    } else {
      console.log(`Logged with phone. Token: ${JSON.stringify(token)}`);
      
      var getResults = await this.sendRequestForPhoneNumber(token.code);
     console.log('Here is the verified phone number',getResults);
    //  this.setState({
    //     phoneNumber: getResults.phone.number
    //  })
    }
  })
              // Configures the SDK with some options
              return(
              <AppNavigator />
                );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
